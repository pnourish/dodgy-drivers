﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine;

public class CameraScript : MonoBehaviour {

    public GameObject[] cars;
    GameObject desiredCar;
    GameObject firstPlace;

    public GameObject first;
    public GameObject second;
    public GameObject third;
	public GameObject fourth;
    public GameObject winner;

    int numCarsLeft;

    private Vector3 desiredPos;

    public float smoothTime = 1000F;
    private Vector3 velocity = Vector3.zero;

    private float startRaceTimer;
    private int intStartRaceTimer;
    private bool raceStarted;
    public Text timerText;
    // Use this for initialization
    void Start ()
    {
        timerText.gameObject.SetActive(true);
        startRaceTimer = 4;
        raceStarted = false;
        SetTimerText();
        first.SetActive(false);
        second.SetActive(false);
        third.SetActive(false);
        fourth.SetActive(false);

        SetIDs();
    }

    // Update is called once per frame
    void Update ()
    {
        if (startRaceTimer > 1)
        {
            startRaceTimer -= Time.deltaTime;
            SetTimerText();
			if (startRaceTimer > 3 && startRaceTimer < 4)
			{
                first.SetActive(true);
            }
            if (startRaceTimer > 2 && startRaceTimer < 3)
            {
                second.SetActive(true);

            }

            if (startRaceTimer > 1 && startRaceTimer < 2)
            {
                third.SetActive(true);

            }
        }
        if (startRaceTimer <= 1 && raceStarted == false)
        {
            fourth.SetActive(true);

            cars[0].GetComponent<ControllerPlayerMovement>().SetCanMove(true);
            cars[1].GetComponent<ControllerPlayerMovement>().SetCanMove(true);

            Debug.Log("Race Started");
            raceStarted = true;
            timerText.gameObject.SetActive(false);

        }

        UpdateCameraPos();
            CheckHowManyCarsLeft();
            if (numCarsLeft == 1)
            {
                FindWinner();
                //Load the Game Over screen here
                SceneManager.LoadScene(2);
            }
    }

    GameObject FindFirstPlace()
    {
        firstPlace = cars[0];
        for (int i = 0; i < cars.Length; i++)
        {
            if (cars[i].transform.position.z >= firstPlace.transform.position.z)
            {

                firstPlace = cars[i];
                desiredCar = cars[i];
            }
        }
        return desiredCar;
    }

    void UpdateCameraPos()
    {

        desiredPos = FindFirstPlace().transform.position;

        transform.position = new Vector3(transform.position.x, transform.position.y, desiredPos.z);//Vector3.SmoothDamp(transform.position, desiredPos, ref velocity, smoothTime);


    }

    int CheckHowManyCarsLeft()
    {
        numCarsLeft = 0;
        for (int i = 0; i < cars.Length; i++)
        {
            if (cars[i].active == true)
            {
                numCarsLeft++;
            }
        }
        return numCarsLeft;
    }

    void FindWinner()
    {
        for (int i = 0; i < cars.Length; i++)
        {
            if (cars[i].active == true)
            {
                PlayerPrefs.SetInt("Winner", i);
                //winner = cars[i];
            }
        }
    }

    void SetTimerText()
    {
        intStartRaceTimer = (int)startRaceTimer;
        timerText.text = "Start in: " + intStartRaceTimer.ToString();
    }

    void SetIDs()
    {
        for (int i = 0; i < cars.Length; i++)
        {
            cars[i].GetComponent<ControllerPlayerMovement>().SetPlayerID(i);
        }
    }
}
