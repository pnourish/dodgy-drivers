﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XInputDotNetPure;

public class ControllerPlayerMovement : MonoBehaviour
{

    private int playerID;
    private float forwardMovement;
    private float backwardMovement;
    private float turningMovement;

    public float originalTurningSpeed = 60f; // this value represents how fast the object turns from side to side
    public float currTurningSpeed;

    public float originalMovementSpeed = 10f; // this value represents how fast the object can move
    public float currMovementSpeed;

    private float maxMovementSpeed = 100.0f;
    private float maxTurningSpeed = 300.0f;

    private float speedUpRate;

    float R2Input;
    float L2Input;
    float SteeringInput;

    float totalGameTime;


    private float maxSeenRValue = 0.5f;
    private float maxSeenLValue = 0.5f;

    bool canMove;

    // Use this for initialization
    void Start()
    {
        forwardMovement = 0;
        turningMovement = 0;
        backwardMovement = 0;
        totalGameTime = 0;
        canMove = false;
        currMovementSpeed = originalMovementSpeed;
        currTurningSpeed = originalTurningSpeed;
        speedUpRate = 500;
    }

    // Update is called once per frame
    void Update()
    {
        if (canMove == true)
        {
            totalGameTime += Time.deltaTime;
            IncreaseSpeed(totalGameTime);

            GamePadState state = GamePad.GetState((PlayerIndex)playerID);

            R2Input = state.Triggers.Right;
            if (maxSeenRValue < R2Input) maxSeenRValue = R2Input;
            R2Input = R2Input / maxSeenRValue;


            L2Input = state.Triggers.Left;
            if (maxSeenLValue < L2Input) maxSeenLValue = L2Input;
            L2Input = L2Input / maxSeenLValue;


            SteeringInput = state.ThumbSticks.Left.X;

            Debug.Log(playerID);
            if (playerID == 1)
            {                 // Debug.Log(R2Input + "I am player " + playerID);
                // Debug.Log(L2Input + "I am player " + playerID);
                // Debug.Log(SteeringInput+ "Is the steering input for " + playerID);
                //Debug.Log(state.Triggers.Right);
            }
            if (Mathf.Abs(R2Input) > 0.05f)
            {
                forwardMovement = R2Input * currMovementSpeed * Time.deltaTime;
                transform.Translate(0, 0, forwardMovement); // this line makes the car move forward on the z axis in response to the earlier input
            }

            if (Mathf.Abs(L2Input) > 0.05f)
            {
                backwardMovement = L2Input * currMovementSpeed * Time.deltaTime;
                transform.Translate(0, 0, -backwardMovement); // this line makes the car move backward on the z axis in response to the earlier input
            }

            if (SteeringInput != 0)
            {
                turningMovement = SteeringInput * currTurningSpeed * Time.deltaTime;
                transform.Rotate(0, turningMovement, 0); // this line makes the car turn on the z axis in response to the earlier input
            }
        }
    }


    public void SetPlayerID(int ID)
    {
        playerID = ID;
        //Debug.Log(playerID);
    }

    public void SetCanMove(bool trueOrFalse)
    {
        canMove = trueOrFalse;
        //Debug.Log("Started car " + playerID);
    }

    void IncreaseSpeed(float timePlayed)
    {
       // Debug.Log(timePlayed);
        Debug.Log("this is the speed up rate" + speedUpRate);
        currTurningSpeed += timePlayed / speedUpRate;
        currMovementSpeed += timePlayed / speedUpRate;

        if (currTurningSpeed > maxTurningSpeed)
            currTurningSpeed = maxTurningSpeed;

        if (currMovementSpeed > maxMovementSpeed)
            currMovementSpeed = maxMovementSpeed;
    }
}
