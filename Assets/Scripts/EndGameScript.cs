﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class EndGameScript : MonoBehaviour {
    int winner;


    public Text whoWonText;
	// Use this for initialization
	void Start ()
    {
        winner = PlayerPrefs.GetInt("Winner") + 1;
        Debug.Log(winner);
        SetWinnerText();
	}
	
	// Update is called once per frame
	void Update ()
    {
        Debug.Log(winner);
	}

    void SetWinnerText()
    {
        whoWonText.text = "Player: " + winner.ToString() + " is the winner!";
    }
}
