﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    public string LevelName = "Level1";
    public string ControlsScene = "Controls";

    public AudioClip buttonClickSound;
    public AudioSource source;
    private float volume = 1.0f;
    private float changeScenePauseTime = 1.0f;

    void Start()
    {
        source = GetComponent<AudioSource>();
        
    }

    public void OnPlayClicked()
    {
        StartCoroutine(LoadGameScene(1));
    }

    public void OnControlsClicked()
    {
        StartCoroutine(LoadGameScene(3));
    }

    public void OnCreditsClicked()
    {
        StartCoroutine(LoadGameScene(4));
    }

    public void OnQuitClicked()
    {
        Application.Quit();
    }


    IEnumerator LoadGameScene(int sceneToLoad)
    {

        source.PlayOneShot(buttonClickSound, volume);
        yield return new WaitForSeconds(changeScenePauseTime);
        SceneManager.LoadScene(sceneToLoad);
    }

}