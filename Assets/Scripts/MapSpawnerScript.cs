﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapSpawnerScript : MonoBehaviour {

    public List<GameObject> mapSections;
    public List<GameObject> currSections;

    int numberOfSections;
    int nextSection;
    public float sectionLength;
    private float distanceToNextSection;
    public float timeToSpawnDistance;
    Vector3 instantiationPosition = new Vector3(0, 0, 0);
    Quaternion faceForward;

	// Use this for initialization
	void Start ()
    {
        numberOfSections = mapSections.Count;

	}
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown("space"))
        {
            SpawnNextSection();
        }

        distanceToNextSection = instantiationPosition.z - this.transform.position.z;

        if (distanceToNextSection <= timeToSpawnDistance)
        {
            SpawnNextSection();
        }
        if (currSections.Count > 7)
        {
            DeleteOldestSection();
        }
    }

    void SpawnNextSection()
    {
        nextSection = Random.Range(0, numberOfSections);
        currSections.Add(Instantiate(mapSections[nextSection], instantiationPosition, faceForward));
        instantiationPosition = new Vector3(instantiationPosition.x, instantiationPosition.y, (instantiationPosition.z + sectionLength)); //length of previous section
    }
    void DeleteOldestSection()
    {
        Destroy(currSections[0]);
        currSections.Remove(currSections[0]);
    }

    
}
