﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MeunScripts : MonoBehaviour
{
    public string levelLoad;
    public string menu01;
    public string menu02;
    public void ClickButton(int buttonClicked)
    {
        if (buttonClicked == 1)
        {
            SceneManager.LoadScene(levelLoad);
        }
        if (buttonClicked == 2)
        {
            Application.Quit();
        }
        if (buttonClicked == 3)
        {
            SceneManager.LoadScene(menu02);
        }
        if (buttonClicked == 4)
        {
            SceneManager.LoadScene(menu01);
        }
    }



}
