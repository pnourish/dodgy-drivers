﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerDeathScript : MonoBehaviour {

    Camera GameCamera;

	void Start ()
    {
        GameCamera = FindObjectOfType<Camera>();
    }
	
	void Update ()
    {
        Vector3 screenPos = GameCamera.WorldToScreenPoint(this.transform.position);

        if (screenPos.y <= -50 || transform.position.y < -100)
        {
            this.gameObject.SetActive(false);
        }

    }


}
