﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUps : MonoBehaviour
{
    GameObject Car1;
    GameObject Car2;

    ControllerPlayerMovement carMove1;
    ControllerPlayerMovement carMove2;

    GameObject SpeedBoost;
    public float boostLength = 3;
    public float boostSpeed = 5;

    float currentSpeed;

    float time = 0;
    public float interpolationPeriod = 7;

    //Boost lengths
    float startTime1;
    float endTime1;
    float startTime2;
    float endTime2;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update ()
    {
        time += Time.deltaTime;



        //increase speed over time
        if (time >= interpolationPeriod)
        {
            IncreaseSpeedOverTime();
        }
    }

    void BoostSpeed(GameObject car)
    {
        if (car = Car1)
        {
            startTime1 = Time.deltaTime;
            endTime1 = startTime1 + 3;

            //add boost speed to movement speed
            carMove1.currMovementSpeed = currentSpeed + boostSpeed;
        }
        else if (car = Car2)
        {
            startTime2 = Time.deltaTime;
            endTime2 = startTime2 + 3;

            //add boost speed to movement speed
            carMove2.currMovementSpeed = currentSpeed + boostSpeed;
        }
    }

    void IncreaseSpeedOverTime()
    {
        currentSpeed++;
        time = 0.0f;
    }

    void OnTriggerEnter(Collider other)
    {
        Pickup pickup = other.GetComponent<Pickup>();
        if(pickup != null)
        {
            pickup.OnPickedUp(gameObject);
        }
    }
}
