﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ReturnToGameScript : MonoBehaviour {

    float waitTime;
    bool canReturn;
	// Use this for initialization
	void Start ()
    {
        waitTime = 3;
        canReturn = false;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (canReturn == false)
        waitTime -= Time.deltaTime;
        if (waitTime <= 0)
        {
            if (Input.GetKeyDown("space"))
            {
                Debug.Log("ReturnToGameScript");
                SceneManager.LoadScene(0);
            }
        }      	
	}
}
