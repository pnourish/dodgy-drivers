﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedPickup : Pickup {
    public float speedIncrease = 5.0f;
    public float speedIncreaseTime = 3.0f;

    private float oldSpeed;
    public override void OnPickedUp(GameObject car)
    {
        ControllerPlayerMovement movement = car.GetComponent<ControllerPlayerMovement>();
        oldSpeed = movement.currMovementSpeed;
        movement.currMovementSpeed += speedIncrease;
        Debug.Log("Speed increased!");
        StartCoroutine(ReduceSpeed(movement));

        GetComponent<Renderer>().enabled = false;
        GetComponent<Collider>().enabled = false;
    }

    private IEnumerator ReduceSpeed(ControllerPlayerMovement movement)
    {
        yield return new WaitForSeconds(speedIncreaseTime);
        movement.currMovementSpeed -= speedIncrease;
        Debug.Log("Speed decreased!");
        Destroy(gameObject);
    }
}
